// -----------------
// TestAllocator.cpp
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// -------
// pragmas
// -------

#ifdef __clang__
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#endif

// --------
// includes
// --------

#include "gtest/gtest.h"

#include "Life.hpp"

TEST(LifeFixture, test0) {
    ASSERT_EQ(1, 1);
}
